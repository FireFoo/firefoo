﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{

    public AudioClip GameOverClip;
    public AudioClip BackgroundMusic;
    public AudioSource SoundSource;

    public AudioClip EnemyHit;
    // Start is called before the first frame update
    void Start()
    {
        StartBackgroundMusic();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayGameOver() {
        SoundSource.clip = GameOverClip;
        SoundSource.loop = false;
        SoundSource.Play();
    }

    public void StartBackgroundMusic() {
        SoundSource.clip = BackgroundMusic;
        SoundSource.loop = true;
        SoundSource.Play();
    }

    public void StopBackgroundMusic() {
        SoundSource.Stop();
    }

    public void PlayEnemyHit() {
        SoundSource.PlayOneShot(EnemyHit, 0.6f);
    }
}
