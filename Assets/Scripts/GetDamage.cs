﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDamage : MonoBehaviour
{
    public int health;
    private bool dead = false;
    private bool isTriggerEnabled = true;

    void Start() {
        enabled = false;
    }

    void OnBecameVisible() {
        Debug.Log("Visible");
        enabled = true;
    }

    
    void Update() {
        if (dead) {
            isTriggerEnabled = false;
            dead = false;
        } else {
            isTriggerEnabled = true;
        }
    }

    public void TakeDamage(int damage) {
        health -= damage;

        if (health <= 0) {
            Die();
        }
    }

    private void Die() {
        Destroy(gameObject);
    }

    private IEnumerator Fade() 
    {
        for (float ft = 1f; ft >= 0; ft -= 0.1f) 
        {
            Color c = GetComponent<Renderer>().material.color;
            c.a = ft;
            GetComponent<Renderer>().material.color = c;
            yield return new WaitForSeconds(.1f);
        }
    }
}
