﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

public class AdController : MonoBehaviour
{
    public static AdController instance;
    public static bool AdFinished;
    private string storeId = "3351167";
    private string videoAd = "video";
    private string rewardedVideoAd = "rewardedVideo";

    public static AdController Instance { get { return instance; } }

    private void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        } else {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // TODO: change to false when published
        Monetization.Initialize(storeId, false);
        AdFinished = false;
    }

    // Update is called once per frame
    void Update()
    {
         
    }

    public void ShowVideoAd() {
        if (Monetization.IsReady(videoAd)) {
            ShowAdPlacementContent ad = null;
            ad = Monetization.GetPlacementContent(videoAd) as ShowAdPlacementContent;

            if (ad != null) {
                ad.Show(AdFinishedOrSkipped);
            }
        }
    }

    public void ShowRewardedVideoAd() {
        if (Monetization.IsReady(rewardedVideoAd)) {
            ShowAdPlacementContent ad = null;
            ad = Monetization.GetPlacementContent(rewardedVideoAd) as ShowAdPlacementContent;

            if (ad != null) {
                ad.Show(AdFinishedOrSkipped);
            }
        }
    }

    private void AdFinishedOrSkipped(ShowResult result) {
        Debug.Log("RESULT: " + result);
        if (result == ShowResult.Finished || result == ShowResult.Skipped) {
            AdFinished = true;
        }
    }
}
