﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyFireman : MonoBehaviour
{
    public Transform firePoint;
    public GameObject foamBulletPrefab;
    public GameManager gameManager;
    public float velocity = 1;
    private Rigidbody2D rigidBody;

    public Text healthText;
    public const int maxHealth = 3;
    private int collisions = 0;

    private bool isAlreadyCollided = false;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        PrintHealth(maxHealth);
        ReturnToPosition();
    }

    void Update()
    {
        //Check if the device running this is a desktop
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            if (Input.GetKey("up"))
            {
                rigidBody.velocity = Vector2.up * velocity;
            }

            if (Input.GetKey("right"))
            {
                Shoot();
            }
        }

        //Check if the device running this is a handheld
        if (SystemInfo.deviceType == DeviceType.Handheld)
        {
            
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (Input.touches[i].phase == TouchPhase.Began)
                    {
                        if (Input.touches[i].position.x < 600) 
                        {
                            rigidBody.velocity = Vector2.up * velocity;
                        } else if (Input.touches[i].position.x > 1400) {
                            Shoot();
                        }
                    }
            } 
        }

        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "EscapeGameOver") {
            Debug.Log("COLLIDED WITH ENEMY");
            if (!isAlreadyCollided) {
                collisions += 1;
                isAlreadyCollided = true;
                if (collisions >= maxHealth) {
                    PrintHealth(0);
                    gameManager.GameOver();
                    collisions = 0;
                } else {
                    PrintHealth(maxHealth - collisions);
                    StartCoroutine(Blink(7));
                }   
                               
            }
            ReturnToPosition(); 
        }
    }

    private void Shoot() {
        if (Time.timeScale > 0.5) {
            Instantiate(foamBulletPrefab, firePoint.position, firePoint.rotation);
        }
    }

    private void Die() {
        Destroy(gameObject);
    }

    private IEnumerator Blink(int times) {
        for (int i = 0; i < times; i++){
            GetComponent<Renderer>().enabled = false;
            yield return new WaitForSeconds(0.2f);
            GetComponent<Renderer>().enabled = true;
            yield return new WaitForSeconds(0.2f);
        }
        isAlreadyCollided = false;
    }

    private void PrintHealth(int health) {
        healthText.text = "" + health;
    }

    private void ReturnToPosition()
    {
        gameObject.transform.position = new Vector3(-6.02f, 0.35f, 300);  
    }
}
